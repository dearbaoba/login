var express = require('express');
var router = express.Router();
var basicAuth = require('basic-auth');
var DBTools = require('../modules/dbTools');

// { name: 'foo', pass: 'bar' }
var auth = function (req, res, next) {
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    };

    DBTools.search('users', {uname:user.name, psw:user.pass}, function(data) {
        if(data && data.length > 0) {
            return res.send(data[0]);
        } else {
            return unauthorized(res);
        }
    })
};

router.get('/', auth, function (req, res) {
    res.send(200, 'Authenticated');
});

module.exports = router;