var express = require('express');
var router = express.Router();
var DBTools = require('../modules/dbTools');


// {uname: foo, psw:bar}
router.get('/', function(req, res, next) {
    DBTools.search('users', {uname:req.query['uname']}, function(data) {
        if(data && data.length == 0) {
            req.query['reg_time'] = (new Date()).getTime();
            DBTools.insert('users', req.query, function(data) {
                res.send('ok');
            })
        } else {
            res.send('exist user');
        }
    })
});

module.exports = router;